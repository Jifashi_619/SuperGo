import torch.nn as nn
import torch.nn.functional as F
import torch


# 策略器
class PolicyNet(nn.Module):
    """
    This network is used in order to predict which move has the best potential to lead to a win
    given the same 'state' described in the Feature Extractor model.
    """

    def __init__(self, inplanes, outplanes):
        super(PolicyNet, self).__init__()
        self.outplanes = outplanes

        # 2d卷积运算
        self.conv = nn.Conv2d(inplanes, 1, kernel_size=1)

        # 使特征图满足均值为0 方差为1的分布规律？
        self.bn = nn.BatchNorm2d(1)
        # 加快运算效率 防止溢出
        self.logsoftmax = nn.LogSoftmax(dim=1)

        # 全连接层
        self.fc = nn.Linear(outplanes - 1, outplanes)

    # 前向传播 求出梯度?  这个函数应该是自己重写 torch自动调用
    def forward(self, x):
        """
        x : feature maps extracted from the state
        probas : a NxN + 1 matrix where N is the board size
                 Each value in this matrix represent the likelihood
                 of winning by playing this intersection
        """

        # 这里就是以函数的方式调用relu 编程风格不同
        x = F.relu(self.bn(self.conv(x)))
        x = x.view(-1, self.outplanes - 1)
        x = self.fc(x)
        probas = self.logsoftmax(x).exp()

        return probas
